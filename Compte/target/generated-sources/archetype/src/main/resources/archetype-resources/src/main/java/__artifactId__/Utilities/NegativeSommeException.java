#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.Utilities;

public class NegativeSommeException extends Exception {

	public NegativeSommeException(String msg) {
		super(msg);
	}

}
