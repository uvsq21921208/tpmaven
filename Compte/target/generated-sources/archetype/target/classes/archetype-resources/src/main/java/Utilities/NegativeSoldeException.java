#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.Utilities;

public class NegativeSoldeException extends Exception {

 public NegativeSoldeException(String msg) {
  super(msg);
 }

}