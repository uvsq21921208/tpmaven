package fr.uvsq21921208.Compte.Utilities;

public class NegativeSoldeException extends Exception {

 public NegativeSoldeException(String msg) {
  super(msg);
 }

}