package fr.uvsq21921208.Compte.Utilities;

public class NegativeSommeException extends Exception {

	public NegativeSommeException(String msg) {
		super(msg);
	}

}
