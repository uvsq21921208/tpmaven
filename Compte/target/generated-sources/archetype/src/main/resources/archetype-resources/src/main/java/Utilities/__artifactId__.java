#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.Utilities;

public class ${artifactId} {

 private double solde;

 /**
  * initialiser l'objet compte par un solde initiale
  * 
  * @param solde : solde courrant.
  */

 public ${artifactId}(double solde) {
  this.solde = solde;
 }

 /**
  * retourner le solde actuel du compte
  * 
  * @return le solde actuel du compte
  */
 public double getSolde() {
  return this.solde;
 }

 /**
  * changer le solde d'un compte
  * 
  * @param solde le nouveau solde d'un compte
  */
 private void setSolde(double solde) {

  this.solde = solde;
 }

 /**
  * Ajouter  somme au solde actuel du compte
  * 
  * @param somme la somme d'argent à créditer dans le solde actuel
  * @throws NegativeSommeException déclenchée si la somme à créditer est
  *                                négative
  */
 public void crediter(double somme) throws NegativeSommeException {
  if (somme > 0)
   solde += somme;
  else
   throw new NegativeSommeException("somme invalide");
 }

 /**
  * Enlever somme au solde actuel du compte
  * 
  * @param somme la somme d'argent à débiter du solde actuel
  * @throws NegativeSoldeException    déclenchée si la somme à créditer est plus
  *                                   grande que le solde actuel
  * @throws NegativeSommeException est déclenchée si la somme à débiter est
  *                                   négative
  */
 public void debiter(double somme) throws NegativeSoldeException, NegativeSommeException {
  if (somme > 0) {
   if (somme < solde)
    solde -= somme;
   else
    throw new NegativeSoldeException(
     "la somme que vous voulez débiter est inferieur à votre solde actuelle");
  } else
   throw new NegativeSommeException("somme invalide");
 }

 /**
  * effectuer un virement à 'compte' de somme 'somme'
  * 
  * @param compte le compte distinateur où le viremet sera effectué
  * @param somme  la somme à verser dans compte
  * @throws NegativeSoldeException déclenchée si la somme à verser dans compte
  *                                est plus grande que le solde actuel
  * @throws NegativeSommeException déclenchée si la somme à verser est négative
  */
 public void virement(${artifactId} compte, double somme) throws NegativeSoldeException, NegativeSommeException {
  double d_solde = compte.getSolde();
  debiter(somme);
  compte.setSolde(d_solde + somme);

 }

}