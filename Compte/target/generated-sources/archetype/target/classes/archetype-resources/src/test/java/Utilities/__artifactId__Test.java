#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.Utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


public class ${artifactId}Test {

	// creer deux nouveaux objets de type compte
	${artifactId} compte1 = new ${artifactId}(1000.0);
	${artifactId} compte2 = new ${artifactId}(550.60);

	// tester le contenu d'un compte
	@Test
	public void test${artifactId}() {

		assertEquals(compte1.getSolde(), 1000.0, 0);
		assertEquals(compte2.getSolde(), 550.60, 0);

	}

	// tester la méthode crediter
	@Test
	public void testCrediter() throws NegativeSommeException {
		compte1.crediter(50.0);
		compte2.crediter(100);
		assertTrue(compte1.getSolde()==1000.0+50.0);
		assertEquals(compte2.getSolde(), 650.60, 0);
		
	}

	// tester la méthode crediter
	@Test
	public void testDebiter() throws NegativeSommeException, NegativeSoldeException {
		compte1.debiter(500.0);
		compte2.debiter(100.0);
		assertEquals(compte2.getSolde(), 550.60 - 100, 0);
		assertTrue(compte1.getSolde()==1000.0-500.0);
		assertEquals(compte1.getSolde(), 1000.0 - 500.0, 0);
	}

	// tester la méthode virement
	@Test
	public void testVirementr() throws NegativeSommeException, NegativeSoldeException {
		compte1.virement(compte2, 550.0);
		assertEquals(compte1.getSolde(), 1000.0 - 550.0, 0);
		assertEquals(compte2.getSolde(), 550.60 + 550.0, 0);
	}

	// tester la generation de l'exception NegativeSommeException
	@Test(expected = NegativeSommeException.class)
	public void negativeSommeException() throws Exception {
		compte1.debiter(500.0);
		compte1.crediter(-600);
		compte2.virement(compte1, 700);

	}

	// tester la generation de l'exception NegativeSoldeException
	@Test(expected = NegativeSoldeException.class)
	public void NegativeSoldeException() throws Exception {
		compte1.debiter(600.0);
		compte1.virement(compte2, 700);
	

	}

}
