#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.Utilities;

public class NegativeSoldeException extends Exception {

 public NegativeSoldeException(String msg) {
  super(msg);
 }

}