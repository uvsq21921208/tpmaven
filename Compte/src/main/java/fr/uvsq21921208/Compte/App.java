package fr.uvsq21921208.Compte;

import fr.uvsq21921208.Compte.Utilities.Compte;
import fr.uvsq21921208.Compte.Utilities.NegativeSommeException;

public class App {
 public static void main(String[] args) throws NegativeSommeException {

  Compte compte1 = new Compte(1000.0);
  Compte compte2 = new Compte(550.60);
  compte1.crediter(50.0);
  compte2.crediter(100);

 }
}