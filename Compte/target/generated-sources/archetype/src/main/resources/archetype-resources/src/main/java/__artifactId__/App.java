#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId};

import ${package}.${artifactId}.Utilities.${artifactId};
import ${package}.${artifactId}.Utilities.NegativeSommeException;

public class App {
 public static void main(String[] args) throws NegativeSommeException {

  ${artifactId} compte1 = new ${artifactId}(1000.0);
  ${artifactId} compte2 = new ${artifactId}(550.60);
  compte1.crediter(50.0);
  compte2.crediter(100);

 }
}